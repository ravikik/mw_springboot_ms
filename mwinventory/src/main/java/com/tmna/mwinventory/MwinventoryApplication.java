package com.tmna.mwinventory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MwinventoryApplication {

	public static void main(String[] args) {
		SpringApplication.run(MwinventoryApplication.class, args);
	}

}
