package com.tmna.mwinventory.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.tmna.mwinventory.model.Apps;



//Annotate with Repository tag

@Repository

/*Extend the queueRepository with Mongo Repository passing in the class Queue with Identifier as String
 * Extending Mongo Repository allows to perform CRUD operations on the Mongo DB
 */


public interface Apprepo extends MongoRepository<Apps, String>{
	// Create two methods to retrieve the Queues using name and Owner
	public Apps findByAppName(String appName);
	
	public Apps findByAppId(long AppId);
	
	

}