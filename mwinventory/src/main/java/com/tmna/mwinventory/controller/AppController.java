package com.tmna.mwinventory.controller;


import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tmna.mwinventory.model.Apps;

import com.tmna.mwinventory.service.AppService;



	@CrossOrigin(origins = "http://localhost:3000")
	@RestController
	@RequestMapping("/api/v1/Apps")
	public class AppController {
		@Autowired
		private AppService AppService;

		

		@PostMapping("/addAppl")
		public Apps addApp(@RequestBody Apps newApp) {
			return AppService.addApp(newApp);
			
		} 

		@GetMapping("/getAllAppl")
		public List<Apps> retrieveAll() {
			return AppService.getAll();
		}
		
		@GetMapping("/getApp")
		public Apps getbyAppName(@RequestParam String appName) {
		  return AppService.getByAppName(appName);
		  
	    }
		
		//@RequestMapping(method= RequestMethod.PUT)
		@PutMapping("/updateApp")
		public Apps updateApp(@RequestBody Apps updateApp) {
		//Queues updateQueue = QueueRepository.findByid(updateQueuedetails.getId());
		
	    return AppService.updateApp(updateApp);

	    
	    
			
		}
		
		@PostMapping("/deleteAllApps")
		public String deleteAll() {
			AppService.deleteAll();
			return "Deleted All Apps";
		}
		
		@PostMapping("/deleteApp")
		public String deleteByAppName(@RequestBody String appName) {
			AppService.deleteByappName(appName);
			return "Deleted App : " +appName;
		}
		}

	
	

