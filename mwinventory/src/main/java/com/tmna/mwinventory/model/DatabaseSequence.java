package com.tmna.mwinventory.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Database_Sequence")
public class DatabaseSequence {
	
	@Id
	private String id;
	private long seq;
	public String getid() {
		return id;
	}
	public void setId(String id) {
		id = id;
	}
	public long getSeq() {
		return seq;
	}
	public void setSeq(long seq) {
		this.seq = seq;
	}
	

}