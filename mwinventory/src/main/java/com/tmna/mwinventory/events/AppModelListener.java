package com.tmna.mwinventory.events;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.BeforeConvertEvent;
import org.springframework.stereotype.Component;

import com.tmna.mwinventory.model.Apps;
import com.tmna.mwinventory.service.SequenceGeneratorService;

@Component
public class AppModelListener extends AbstractMongoEventListener<Apps>{
	
    private SequenceGeneratorService sequenceGenerator;

    @Autowired
    public AppModelListener(SequenceGeneratorService sequenceGenerator) {
        this.sequenceGenerator = sequenceGenerator;
    }

    @Override
    public void onBeforeConvert(BeforeConvertEvent<Apps> event) {
        if (event.getSource().getAppId() < 1) {
            event.getSource().setAppId(sequenceGenerator.generateSequence(Apps.SEQUENCE_NAME));
        }
    }

}
