package com.tmna.mwinventory.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.tmna.mwinventory.model.Apps;
import com.tmna.mwinventory.repository.Apprepo;


@Service
public class AppService {
	
	@Autowired
	
	// Create a private variable to hold reference to our repository
	private Apprepo AppRepo;
	 
	// Implement CRUD service methods to Create , Retrieve , Update and Delete to Mongo DB
	
	
	// Implement Create CRUD Service
	 
	 public Apps addApp(Apps newApp) {
		 
		 return AppRepo.save(newApp);
	 }
	
	 // Implement Retrieve Operation
	 
	 public List<Apps> getAll(){
		 return AppRepo.findAll();
	 }
	
	 // Implement Retrieve Operation using the queueName
	 
	 public Apps getByAppName(String appName){
		 
		 return AppRepo.findByAppName(appName);
		 
	 }
	
	// Implement Update Operation using 
	 
	 public Apps updateApp(Apps updateApp) {
     return AppRepo.save(updateApp);
				 
	 }
	
	// Implement Delete Method
	 
	 public void deleteByappName(String appName) {
		 Apps q = AppRepo.findByAppName(appName);
		 AppRepo.delete(q);
	 }

	 // Implement Delete All Method
	 
	 public void deleteAll() {
		 
		 AppRepo.deleteAll();
	 }
}

